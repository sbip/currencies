const NotFound = () => {
    return(
        <h2 style={{textAlign: "center", fontSize: 50}}>Page not found!</h2>
    )
}
export default NotFound;