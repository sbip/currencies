import axios from 'axios'
import authHeader from "./auth-header";

class CurrenciesService {

    getInfo() {
        return axios.get(process.env.REACT_APP_API_URL + 'currencies/info');
    }

    getCurrentExchangeRates() {
        return axios.get(process.env.REACT_APP_API_URL + 'currencies/currentExchangeRates', {headers: authHeader()});
    }

    getExchangeRates(currencyList, startDateInput, endDateInput) {
        let init = [];
        if (startDateInput) init.push(['startDateInput', startDateInput]);
        if (endDateInput) init.push(['endDateInput', endDateInput]);
        init.push(['currencyList', currencyList]);
        const params = new URLSearchParams(init);

        return axios.get(process.env.REACT_APP_API_URL + 'currencies/exchangeRates', {params, headers: authHeader()});
    }
}

export default new CurrenciesService();