import axios from 'axios'
import authHeader from "./auth-header";

class HistoryService {

    getHistory() {
        return axios.get(process.env.REACT_APP_API_URL + 'history',{headers: authHeader()});
    }

}

export default new HistoryService();