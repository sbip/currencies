import CurrenciesService from "../../services/CurrenciesService";

const InfoActionType = {
    SUCCESS: "INFO_SUCCESS",
    FAIL: "INFO_FAIL"
};

const InfoAction = () => {
    return async (dispatch) => {
        CurrenciesService.getInfo()
            .then(value => {
                dispatch({type: InfoActionType.SUCCESS, payload: value.data});
            })
            .catch((error) =>
                dispatch({
                    type: InfoActionType.FAIL, payload: error.response.data.message,
                })
            )
    };
};

export {
    InfoActionType,
    InfoAction
};
