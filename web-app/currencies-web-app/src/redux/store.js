import {applyMiddleware, combineReducers, createStore} from "redux";
import authReducer from "./reducers/AuthReducer";
import authErrorReducer from "./reducers/AuthErrorReducer";
import infoReducer from "./reducers/InfoReducer";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";
import currentExchangeRatesReducer from "./reducers/CurrentExchangeRatesReducer";

const rootReducer = combineReducers({
    authState: authReducer,
    authError: authErrorReducer,
    infoState: infoReducer,
    currentExchangeRatesState: currentExchangeRatesReducer
});

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

export default store;
