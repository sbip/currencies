const infoReducer = (state = null, action) => {
    switch (action.type) {

        case "INFO_SUCCESS":
            return action.payload;

        default:
            return state;
    }
};

export default infoReducer;
