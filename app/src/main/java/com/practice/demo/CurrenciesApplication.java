package com.practice.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.StopWatch;

@Slf4j
@SpringBootApplication
@EnableScheduling
@RequiredArgsConstructor
public class CurrenciesApplication implements ApplicationRunner {

	private final Scheduler scheduler;


	public static void main(String[] args) {
		SpringApplication.run(CurrenciesApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		StopWatch watch = new StopWatch();
		watch.start();
		scheduler.calculateMonthlyRatesAverage();
		watch.stop();
		log.info("Time Elapsed for calculate monthly average in seconds: {}", watch.getTotalTimeSeconds());
	}
}
