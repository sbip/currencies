package com.practice.demo.service;

import com.practice.demo.dto.*;
import com.practice.demo.dto.Rate;
import com.practice.soap.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class MNBMapper {

    public Currencies mapCurrenciesInfo(MNBCurrencies mnbCurrencies) {

        List<String> currencyList = mnbCurrencies.getCurrencies().stream()
                .map(Curr::getCurr)
                .collect(Collectors.toList());

        return Currencies.builder()
                .firstDate(mnbCurrencies.getFirstDate().getFirstDate())
                .lastDate(mnbCurrencies.getLastDate().getLastDate())
                .currencies(currencyList)
                .build();
    }

    public CurrentExchangeRates currentExchangeRates(MNBCurrentExchangeRates mnbCurrentExchangeRates) {

        return CurrentExchangeRates.builder()
                .date(mnbCurrentExchangeRates.getDay().getDate())
                .rates(mnbCurrentExchangeRates.getDay().getRate().stream()
                        .map(this::mapRate)
                        .collect(Collectors.toList())
                )
                .build();
    }

    private Rate mapRate(com.practice.soap.dto.Rate rate) {
        return Rate.builder()
                .currency(rate.getCurr())
                .unit(rate.getUnit())
                .value(rate.getValue())
                .build();
    }

    public ExchangeRates exchangeRates(MNBExchangeRates mnbExchangeRates) {

        // log.info("mnb: " + mnbExchangeRates);

        Map<String, Pair<String, List<DailyValue>>> map = new HashMap<>();

        mnbExchangeRates.getDay()
                .stream()
                .filter(day -> day != null && day.getRate() != null)
                .forEach(day -> day.getRate()
                        .forEach(rate -> {
                            String curr = rate.getCurr();
                            map.putIfAbsent(curr, Pair.of(rate.getUnit(), new ArrayList<>()));
                            String date = day.getDate();
                            String value = rate.getValue();
                            map.get(curr).getSecond().add(buildDailyValue(date, value));
                            log.debug("" + map.get(curr));
                        })
                );

        ExchangeRates exchangeRates = ExchangeRates.builder().exchangeRates(new ArrayList<>()).build();
        map.forEach((key, value) -> {
            ExchangeRate exchangeRate = ExchangeRate.builder()
                    .unit(value.getFirst())
                    .currency(key)
                    .dailyValues(value.getSecond())
                    .build();

            exchangeRates.getExchangeRates().add(exchangeRate);
        });
        return exchangeRates;
    }

    private DailyValue buildDailyValue(String date, String value) {

        return DailyValue.builder()
                .date(date)
                .value(value)
                .build();
    }


}
