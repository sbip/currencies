package com.practice.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Currencies {

    @ApiModelProperty(notes = "The first date of the time interval that can be queried")
    String firstDate;

    @ApiModelProperty(notes = "The last date of the time interval that can be queried")
    String lastDate;

    @ApiModelProperty(notes = "The list of all queriable currencies")
    List<String> currencies;
}
