package com.practice.demo.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DailyValue {

    String date;
    String value;
}
