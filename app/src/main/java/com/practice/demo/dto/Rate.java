package com.practice.demo.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Rate {

    String unit;
    String currency;
    String value;
}
