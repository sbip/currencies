package com.practice.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "history")
public class History {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "timestamp")
    private String timestamp;

    @Column(name = "currencies")
    private String currencies;

    @Column(name = "startdate")
    private String startDate;

    @Column(name = "enddate")
    private String endDate;

    public History(String username, String timestamp, String currencies, String startDate, String endDate) {
        this.username = username;
        this.timestamp = timestamp;
        this.currencies = currencies;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}



