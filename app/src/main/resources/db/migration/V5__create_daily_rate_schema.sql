CREATE TABLE IF NOT EXISTS DAILY_RATE
(
    ID       SERIAL PRIMARY KEY,
    CURRENCY VARCHAR(3)   NOT NULL,
    RATE     VARCHAR(255) NOT NULL,
    DATE     DATE         NOT NULL

);